package com.backbase.kalah.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Setter
@Getter
public class MoveGameResponse {
    private String id;
    private String url;
    private Map<Integer, Integer> status;
}
