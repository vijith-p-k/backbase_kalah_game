package com.backbase.kalah.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateGameResponse {
    private String id;
    private String uri;
}
