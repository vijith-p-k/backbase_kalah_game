package com.backbase.kalah.domain.exception;

public class GameException extends RuntimeException{
    public GameException(String message) {
        super(message);
    }
}
