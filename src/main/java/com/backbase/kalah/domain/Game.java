package com.backbase.kalah.domain;

import com.backbase.kalah.domain.board.GameBoard;
import com.backbase.kalah.domain.model.GameContext;

import java.util.List;
import java.util.UUID;

public class Game {

    private GameBoard gameBoard;

    public String startGame() {
        GameContext gameContext = new GameContext();
        this.gameBoard = new GameBoard(gameContext);
        this.gameBoard.initialize(6);
        return UUID.randomUUID().toString();
    }

    public List<Integer> makeMove(int pitId) {
        this.gameBoard.move(pitId - 1);
        return this.gameBoard.getPits().getPitStones();
    }


}
