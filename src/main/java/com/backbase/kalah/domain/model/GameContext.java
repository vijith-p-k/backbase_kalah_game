package com.backbase.kalah.domain.model;

import com.backbase.kalah.domain.constants.Player;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameContext {
    private int currentPick;
    private Player currentPlayer;


    public GameContext() {
        this.currentPlayer = Player.FIRST;
    }
}
