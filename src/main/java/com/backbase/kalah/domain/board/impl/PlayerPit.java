package com.backbase.kalah.domain.board.impl;

import com.backbase.kalah.domain.board.Pit;
import com.backbase.kalah.domain.constants.Player;
import com.backbase.kalah.domain.constants.Result;
import com.backbase.kalah.domain.model.GameContext;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class PlayerPit implements Pit {

    private Player player;
    private GameContext gameContext;
    private int stones;

    PlayerPit(GameContext gameContext, Player player){
        this.player = player;
        this.gameContext = gameContext;
    }

    PlayerPit(int gameType, GameContext gameContext, Player player){
        this.player = player;
        this.gameContext = gameContext;
        this.stones = gameType;
    }

    @Override
    public Result place(int stones) {
        this.stones = this.stones + stones;
        return Result.PLACED;
    }
}
