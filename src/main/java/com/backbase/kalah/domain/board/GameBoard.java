package com.backbase.kalah.domain.board;

import com.backbase.kalah.domain.board.impl.PlayerEndzonePit;
import com.backbase.kalah.domain.board.impl.PlayerHousePit;
import com.backbase.kalah.domain.constants.Player;
import com.backbase.kalah.domain.constants.Result;
import com.backbase.kalah.domain.model.GameContext;
import lombok.Getter;
import lombok.Setter;

import javax.xml.ws.Holder;
import java.util.stream.IntStream;

@Getter
@Setter
public class GameBoard {

    private GameContext gameContext;
    private Pits pits;

    public GameBoard(GameContext gameContext) {
        this.gameContext = gameContext;
    }


    public void initialize(int gameType) {
        pits = new Pits();
        final Holder<Player> player = new Holder<>(Player.FIRST);
        IntStream.range(0, 14).peek(i -> {
            Pit pit;
            if (i == Player.FIRST.getValue() || i == Player.SECOND.getValue()) {
                pit = new PlayerEndzonePit(gameContext, player.value);
                player.value = Player.SECOND;
            } else if (i > 6) {
                PlayerHousePit playerHousePit = new PlayerHousePit(gameType, gameContext, player.value);
                OpponentAwarePit oppositePit = (OpponentAwarePit) pits.searchByIndex(
                        (i - ((i - Player.FIRST.getValue()) * 2))).item;
                playerHousePit.setOpponentPit(oppositePit);
                oppositePit.setOpponentPit(playerHousePit);
                pit = playerHousePit;
            }
            else {
                pit = new PlayerHousePit(gameType, gameContext, player.value);
            }
            pits.insert(pit);
        }).count();

    }


    public void move(int pitId) {
        Pits.Node node = this.pits.searchByIndex(pitId);
        gameContext.setCurrentPick(node.item.pick());
        Result result = Result.PLACED;
        while (node.iterate()) {
            node = node.next;
            result = node.item.place(1);
            if (result != Result.SKIP)
                gameContext.setCurrentPick(gameContext.getCurrentPick() - 1);

            if (gameContext.getCurrentPick() == 0)
                break;
        }

        evaluateResult(result, (OpponentAwarePit) node.item);

    }

    private void evaluateResult(Result result, OpponentAwarePit opponentAwarePit) {
        switch (result) {
            case BONUS:
                int stones = opponentAwarePit.getBonous();
                this.pits.searchByIndex(gameContext.getCurrentPlayer().getValue()).item.place(stones);
                break;
            case PLACED:
                gameContext.setCurrentPlayer(
                        gameContext.getCurrentPlayer() == Player.FIRST ? Player.SECOND : Player.FIRST);
                break;
            case PLACED_ENDZONE:
                break;
        }
    }

}
