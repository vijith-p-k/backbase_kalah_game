package com.backbase.kalah.domain.board.impl;

import com.backbase.kalah.domain.constants.Player;
import com.backbase.kalah.domain.constants.Result;
import com.backbase.kalah.domain.exception.GameException;
import com.backbase.kalah.domain.model.GameContext;

public class PlayerEndzonePit extends PlayerPit {

    public PlayerEndzonePit(GameContext gameContext, Player player) {
        super(gameContext, player);
    }

    @Override
    public int pick() {
        throw new GameException("Cannot pick from an endzone");
    }

    @Override
    public Result place(int stones) {
        if(!getPlayer().equals(getGameContext().getCurrentPlayer())) {
            return Result.SKIP;
        }

        if(getGameContext().getCurrentPick() - stones == 0) {
            super.place(stones);
            return Result.PLACED_ENDZONE;
        }

        return super.place(stones);
    }
}
