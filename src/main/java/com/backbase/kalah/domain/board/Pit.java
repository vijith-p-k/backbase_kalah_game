package com.backbase.kalah.domain.board;

import com.backbase.kalah.domain.constants.Result;

public interface Pit {
    int pick();
    Result place(int stones);
    int getStones();
}
