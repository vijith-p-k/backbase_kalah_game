package com.backbase.kalah.domain.board;

public interface OpponentAwarePit {
    void setOpponentPit(OpponentAwarePit opponentPit);
    int loot();
    int getBonous();
}
