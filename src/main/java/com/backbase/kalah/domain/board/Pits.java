

package com.backbase.kalah.domain.board;

import com.backbase.kalah.domain.exception.GameException;

import javax.xml.ws.Holder;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Pits {

    private Node head;
    private int size = 0;

    public void insert(Pit value) {
        Node newNode = new Node(value);
        if (null == head) {
            /* When list is empty */
            head = newNode;
        } else {
            Node temp = head;
            while (temp.next != head) {
                temp = temp.next;
            }
            temp.next = newNode;
        }
        newNode.next = head;
        size++;
    }

    public int getSize() {
        return size;
    }

    public List<Integer> getPitStones() {
        List<Integer> pitValues = new ArrayList<>();
        final Holder<Node> temp = new Holder<>(head);
        IntStream.range(0, size).peek(i -> {
            pitValues.add(temp.value.item.getStones());
            temp.value = temp.value.next;
        }).count();

        return pitValues;
    }

    public Node searchByIndex(int index) {
        if (index < 0 || index >= size) {
            throw new GameException("Index is Invalid");
        }
        final Holder<Node> temp = new Holder<>(head);
        IntStream.range(0, index).peek(i -> temp.value = temp.value.next).count();
        return temp.value;
    }

    public class Node {

        Pit item;
        /* Pointer to next node */
        Node next;

        /* Constructor to create a node */
        public Node(Pit item) {
            this.item = item;
        }

        boolean iterate() {
            return true;
        }
    }

}