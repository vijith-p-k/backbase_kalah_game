package com.backbase.kalah.domain.board.impl;

import com.backbase.kalah.domain.board.OpponentAwarePit;
import com.backbase.kalah.domain.constants.Player;
import com.backbase.kalah.domain.constants.Result;
import com.backbase.kalah.domain.exception.GameException;
import com.backbase.kalah.domain.model.GameContext;

public class PlayerHousePit extends PlayerPit implements OpponentAwarePit {

    private OpponentAwarePit oppositePit;

    public PlayerHousePit(int gameType, GameContext gameContext, Player player) {
        super(gameType, gameContext, player);
    }

    @Override
    public int pick() {
        if(!getPlayer().equals(getGameContext().getCurrentPlayer())) {
            throw new GameException("You cannot use opponents house");
        }

        int stones = getStones();
        if(stones == 0) {
            throw new GameException("House is empty");
        }
        setStones(0);
        return stones;
    }

    @Override
    public Result place(int stones) {
        if(getStones() == 0 &&
                getPlayer().equals(getGameContext().getCurrentPlayer())) {
            super.place(stones);
            return Result.BONUS;
        }
        return super.place(stones);
    }

    @Override
    public void setOpponentPit(OpponentAwarePit opponentPit) {
        this.oppositePit = opponentPit;
    }

    @Override
    public int getBonous() {
        int stones = getStones();
        setStones(0);
        return stones + oppositePit.loot();
    }

    @Override
    public int loot() {
        int stones = getStones();
        setStones(0);
        return stones;
    }

    @Override
    protected void finalize() throws Throwable {
        this.oppositePit = null;
        super.finalize();
    }
}
