package com.backbase.kalah.domain.constants;

public enum Player {
    FIRST(6),
    SECOND(13);

    private int value;

    Player(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
