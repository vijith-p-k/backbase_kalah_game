package com.backbase.kalah.domain.constants;

public enum Result {
    PLACED,
    PLACED_ENDZONE,
    SKIP,
    BONUS
}
