package com.backbase.kalah.controller;

import com.backbase.kalah.dto.CreateGameResponse;
import com.backbase.kalah.dto.MoveGameResponse;
import com.backbase.kalah.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/games")
public class GameController {

    private final GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping
    public ResponseEntity<CreateGameResponse> startGame() {
        CreateGameResponse createGameResponse = this.gameService.startGame();
        return new ResponseEntity<>(createGameResponse, HttpStatus.CREATED);
    }

    @PutMapping("/{gameId}/pits/{pitId}")
    public ResponseEntity<MoveGameResponse> move(@PathVariable String gameId, @PathVariable Integer pitId) {
        MoveGameResponse moveGameResponse = this.gameService.makeMove(gameId, pitId);
        return new ResponseEntity<>(moveGameResponse, HttpStatus.ACCEPTED);
    }

    @GetMapping("/{gameId}")
    public ResponseEntity<MoveGameResponse> getGame(@PathVariable String gameId) {
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
