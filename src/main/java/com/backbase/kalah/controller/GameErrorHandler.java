package com.backbase.kalah.controller;

import com.backbase.kalah.domain.exception.GameException;
import com.backbase.kalah.domain.exception.GameNotFoundException;
import com.backbase.kalah.dto.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GameErrorHandler {

    @ExceptionHandler(value = GameException.class)
    public ResponseEntity<ErrorResponse> handleGameException(GameException e) {
        return new ResponseEntity<>(new ErrorResponse(5001, e.getMessage()), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(value = GameNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleGameException(GameNotFoundException e) {
        return new ResponseEntity<>(new ErrorResponse(4000, e.getMessage()), HttpStatus.NOT_FOUND);
    }
}
