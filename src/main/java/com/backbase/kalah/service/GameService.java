package com.backbase.kalah.service;

import com.backbase.kalah.controller.GameController;
import com.backbase.kalah.domain.Game;
import com.backbase.kalah.domain.exception.GameNotFoundException;
import com.backbase.kalah.dto.CreateGameResponse;
import com.backbase.kalah.dto.MoveGameResponse;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class GameService {

    //temporary cache
    private Map<String, Game> gameSessions = new HashMap<>();

    public MoveGameResponse makeMove(String gameId, Integer pitId) {
        Game game = gameSessions.get(gameId);
        if(game == null) {
            throw new GameNotFoundException("GameNotFound");
        }
        MoveGameResponse moveGameResponse = new MoveGameResponse();
        moveGameResponse.setId(gameId);
        moveGameResponse.setUrl(getGameUrl(gameId));
        List<Integer> result = game.makeMove(pitId);
        Map<Integer, Integer> status = IntStream.range(0, result.size()).boxed()
                .collect(Collectors.toMap(i -> i + 1, result::get));
        moveGameResponse.setStatus(status);
        return moveGameResponse;
    }

    public CreateGameResponse startGame() {
        Game game = new Game();
        String id = game.startGame();
        gameSessions.put(id, game);

        CreateGameResponse createGameResponse = new CreateGameResponse();
        createGameResponse.setId(id);
        createGameResponse.setUri(getGameUrl(id));

        return createGameResponse;
    }


    private String getGameUrl(String id) {
        return ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(GameController.class)
                .getGame(id)).toString();
    }

}
