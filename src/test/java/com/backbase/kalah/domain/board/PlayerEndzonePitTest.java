package com.backbase.kalah.domain.board;

import com.backbase.kalah.domain.board.impl.PlayerEndzonePit;
import com.backbase.kalah.domain.constants.Player;
import com.backbase.kalah.domain.constants.Result;
import com.backbase.kalah.domain.exception.GameException;
import com.backbase.kalah.domain.model.GameContext;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class PlayerEndzonePitTest {

    private static GameContext gameContext;
    private static PlayerEndzonePit playerEndzonePit;

    @BeforeClass
    public static void initialize() {
        gameContext = new GameContext();
        gameContext.setCurrentPlayer(Player.FIRST);
        playerEndzonePit = new PlayerEndzonePit(gameContext, Player.FIRST);
    }

    @Test(expected = GameException.class)
    public void testPick_SuccessCase() {
        playerEndzonePit.pick();
    }

    @Test
    public void testPlace_whenPlacingPlayersEndzone_returnsResultPlaced() {
        gameContext.setCurrentPlayer(Player.FIRST);
        Assert.assertEquals(Result.PLACED, playerEndzonePit.place(1));
    }

    @Test
    public void testPlace_whenPlacingOtherPlayersEndzone_returnsResultSkip() {
        gameContext.setCurrentPlayer(Player.SECOND);
        Assert.assertEquals(Result.SKIP, playerEndzonePit.place(1));
    }

    @Test
    public void testPlace_whenPlacingTheLastStone_returnsResultPlacedEndzone() {
        gameContext.setCurrentPick(1);
        Assert.assertEquals(Result.PLACED_ENDZONE, playerEndzonePit.place(1));
    }

}
