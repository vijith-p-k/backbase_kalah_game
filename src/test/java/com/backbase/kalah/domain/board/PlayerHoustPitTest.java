package com.backbase.kalah.domain.board;

import com.backbase.kalah.domain.board.impl.PlayerHousePit;
import com.backbase.kalah.domain.constants.Player;
import com.backbase.kalah.domain.constants.Result;
import com.backbase.kalah.domain.exception.GameException;
import com.backbase.kalah.domain.model.GameContext;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class PlayerHoustPitTest {

    private static GameContext gameContext;
    private static PlayerHousePit playerHousePit;

    @BeforeClass
    public static void initialize() {
        gameContext = new GameContext();
        gameContext.setCurrentPlayer(Player.FIRST);
        playerHousePit = new PlayerHousePit(6, gameContext, Player.FIRST);
    }

    @Test(expected = GameException.class)
    public void testPick_whenOpponentsPit_throwGameException() {
        gameContext.setCurrentPlayer(Player.SECOND);
        playerHousePit.pick();
    }

    @Test(expected = GameException.class)
    public void testPick_whenPitIsEmpty_throwGameException() {
        gameContext.setCurrentPlayer(Player.FIRST);
        playerHousePit.setStones(0);
        playerHousePit.pick();
    }

    @Test
    public void testPick_whenSuccessCase_retursSixStones() {
        gameContext.setCurrentPlayer(Player.FIRST);
        playerHousePit.setStones(6);
        Assert.assertEquals(6, playerHousePit.pick());
        Assert.assertEquals(0, playerHousePit.getStones());
    }

    @Test
    public void testPlace_whenPlacingNormalCase_returnsResultPlaced() {
        gameContext.setCurrentPlayer(Player.FIRST);
        playerHousePit.setStones(3);
        Assert.assertEquals(Result.PLACED, playerHousePit.place(1));
    }

    @Test
    public void testPlace_whenPlacingInEmptyPit_returnsResultBonous() {
        gameContext.setCurrentPlayer(Player.FIRST);
        playerHousePit.setStones(0);
        Assert.assertEquals(Result.BONUS, playerHousePit.place(1));
    }

    @Test
    public void testPlace_whenPlacingInOpponentsEmptyPit_returnsResultPlaced() {
        gameContext.setCurrentPlayer(Player.SECOND);
        playerHousePit.setStones(0);
        Assert.assertEquals(Result.PLACED, playerHousePit.place(1));
    }

    @Test
    public void getBonousTest_successCase_returnsStonesFromBothPits() {
        OpponentAwarePit opponentAwarePit = new PlayerHousePit(6, gameContext, Player.SECOND);
        opponentAwarePit.setOpponentPit(playerHousePit);

        playerHousePit.setOpponentPit(opponentAwarePit);

        Assert.assertEquals(playerHousePit.getStones() +
                ((PlayerHousePit) opponentAwarePit).getStones(), playerHousePit.getBonous());
        Assert.assertEquals(0, playerHousePit.getStones());
        Assert.assertEquals(0, ((PlayerHousePit) opponentAwarePit).getStones());
    }

    @Test
    public void lootTest_successCase_returnsStones(){
        Assert.assertEquals(playerHousePit.getStones(), playerHousePit.loot());
    }

}
