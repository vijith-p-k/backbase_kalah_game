package com.backbase.kalah.domain.board;

import com.backbase.kalah.domain.board.impl.PlayerHousePit;
import com.backbase.kalah.domain.constants.Player;
import com.backbase.kalah.domain.exception.GameException;
import com.backbase.kalah.domain.model.GameContext;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class PitsTest {

    private Pits pits = new Pits();

    @Test
    public void testInsert_SuccessCaseListSizeOne() {
        pits.insert(new PlayerHousePit(6, new GameContext(), Player.SECOND));
        Assert.assertEquals(1, pits.getSize());
    }


    @Test
    public void testInsert_SuccessCaseListSizeTwo() {
        pits.insert(new PlayerHousePit(6, new GameContext(), Player.SECOND));
        pits.insert(new PlayerHousePit(6, new GameContext(), Player.SECOND));
        Assert.assertEquals(2, pits.getSize());
    }

    @Test(expected = GameException.class)
    public void testSearchByIndex_whenInvalidIndex_throwsGameException() {
        pits.searchByIndex(-1);
    }

    @Test
    public void testSearchByIndex_SuccessCase_ReturnsCorrectPit() {
        pits.insert(new PlayerHousePit(6, new GameContext(), Player.SECOND));
        pits.insert(new PlayerHousePit(6, new GameContext(), Player.SECOND));
        Assert.assertNotNull(pits.searchByIndex(1));
    }
}
