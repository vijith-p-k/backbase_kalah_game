package com.backbase.kalah.domain.board;

import com.backbase.kalah.domain.constants.Player;
import com.backbase.kalah.domain.model.GameContext;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class GameBoardTest {

    private static GameBoard gameBoard;

    @BeforeClass
    public static void initialize() {
        GameContext gameContext = new GameContext();
        gameContext.setCurrentPick(0);
        gameContext.setCurrentPlayer(Player.FIRST);
        gameBoard = new GameBoard(gameContext);
    }

    @Test
    public void testIntialize_SucessCase() {
        gameBoard.initialize(6);
        Assert.assertEquals(gameBoard.getPits().getSize(), 14);
    }

    @Test
    public void testMove_successCase_switchToNextPlayer() {
        gameBoard.initialize(6);
        gameBoard.move(5);
        Assert.assertEquals(gameBoard.getGameContext().getCurrentPlayer(), Player.SECOND);
    }

}
