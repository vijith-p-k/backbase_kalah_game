package com.backbase.kalah.domain;

import com.backbase.kalah.domain.exception.GameException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {

    private Game game = new Game();

    @Test
    public void startGameTest_successScenario_returnsRandomString() {
        Assert.assertNotNull(game.startGame());
    }

    @Test(expected = GameException.class)
    public void moveTest_WhenPitIdIsZero_throwsGameException() {
        game.startGame();
        game.makeMove(0);
    }

    @Test(expected = GameException.class)
    public void moveTest_WhenOpponentsPitId_throwsGameException() {
        game.startGame();
        game.makeMove(0);
    }

    @Test
    public void moveTest_WhenSuccessCase_returnsAnInteger() {
        game.startGame();
        Assert.assertNotEquals(game.makeMove(4), 0);
    }
}
