package com.backbase.kalah.service;

import com.backbase.kalah.domain.exception.GameNotFoundException;
import com.backbase.kalah.dto.CreateGameResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class GameServiceTest {

    private GameService gameService = new GameService();

    @Test(expected = GameNotFoundException.class)
    public void testMakeMove_whenGameNotFound_throwsGameNotFoundException() {
        gameService.makeMove("asv", 3);
    }


    @Test
    public void testMakeMove_whenSuccessCase_returnsGameMoveResponse() {
        CreateGameResponse createGameResponse = gameService.startGame();
        Assert.assertNotNull(gameService.makeMove(createGameResponse.getId(), 3));
    }

    @Test
    public void testStartGame_whenSuccessCase_gameStoredInSession() {
        CreateGameResponse createGameResponse = gameService.startGame();
        Assert.assertNotNull(gameService.makeMove(createGameResponse.getId(), 2));
    }
}
