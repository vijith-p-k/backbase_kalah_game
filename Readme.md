# Backbase - Kalah Game Test

Kalah - A two player board game. This application provides restful endpoints for playing this game

### Technology used

This application uses the following technologies:

* Java - (version 1.8)
* Spring boot - (version 2.1.4)
* Apache Maven -(version 3.5.2) Build Tool
* junit - Unit Testing framework (version 4.12)

### Application setup

Execute the following maven command

```sh
$ mvn clean install
```

For running the unit tests

```sh
$ mvn test
```

For runnning the application

```sh
$ mvn spring-boot:run
```

### Sample Request and Response

######Start Game  

`curl --header "Content-Type: application/json" 
--request POST http://localhost:8080/games`

Response  
`HTTP code: 201  
Response Body: { "id": "1234", "uri": "http://localhost:8080/games/1234" }`

######Make Move

`curl --header "Content-Type: application/json" \
--request PUT  http://localhost:8080/games/{gameId}/pits/{pitId}`

Response  
`HTTP code: 200
Response Body:
{"id":"1234","url":"http://localhost:8080/games/1234","status":{"1":"4","2":"4","3":"4","4":"4","5":"4","6":"4","7":"0","8":"4","
9":"4","10":"4","11":"4","12":"4","13":"4","14":"0"}}`

### Design Details
 
 | |  |
 | ------ | ------ |
 | Pit | This interface represents pit on a game board.|
 | Pits | Circular LinkedList Implementation for arranging Pits on board. |
 | OpponentAwarePit | Represent a kind of pit which have reference to the pit opposite to it.  |
 | GameBoard | This class is the game board implementation which holds the pits and synchronizes the play|
 | PlayerPit | An abstract Implementation of pit , which is aware of its owner(player) |
 | PlayerEndZonePit | This class represents a players end zone(Mankala) |
 | PlayerHousePit | This class represents a players house pit(Khala), which also implements OpponentAwarePit |
 
 
 | |  |
  | ------ | ------ |
  | GameException | This exception would be thrown if any business logic is not satisfied.|
  | GameNotFoundException | This exception would be thrown if the game with game id is not found in temporary storage. |
 
 
 | |  |
   | ------ | ------ |
   | Player | Enum which represents each players.|
   | Result | Result represents the result of a move |   
 
 | |  |
    | ------ | ------ |
    | GameContext | Model class for holding shared data (board and pit)|
    | Game | This is aggregate root of the game domain. All the game operations happening through this root |
   
   

##### Author
 Vijith P.K
